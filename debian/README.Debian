Custom bouncer for CrowdSec
===========================

Initial configuration
---------------------

The upstream configuration is shipped directly under /etc and registered via
conffiles, and an override file is deployed alongside upon first installation.
The main file can be updated for new upstream releases by answering the usual
dpkg prompt, while the override file is left untouched.

 - upstream: /etc/crowdsec/bouncers/crowdsec-custom-bouncer.yaml
 - override: /etc/crowdsec/bouncers/crowdsec-custom-bouncer.yaml.local

More about overriding values:
  https://docs.crowdsec.net/docs/configuration/crowdsec_configuration/#overriding-values

The override file is created automatically during the first installation, with
two settings:

 - a no-op custom script (/usr/bin/true), which the admin should change;
 - an API key (optional).

By default, this bouncer package pulls the `crowdsec` package (via Recommends)
and the postinst registers the bouncer automatically. If the admin decided not
to install the `crowdsec` package, they are responsible for registering the
bouncer manually and setting up the required parameters in the override file
(`api_url` and `api_key`).

When the autoregistration happens, the bouncer identifier is stored alongside,
so that the bouncer can unregister itself automatically upon purge:

 - identifier: /etc/crowdsec/bouncers/crowdsec-custom-bouncer.yaml.id


Modifying configuration
-----------------------

Admins are expected to at least change the `bin_path` parameter in the
override file, and to restart the systemd unit afterwards:

    editor /etc/crowdsec/bouncers/crowdsec-custom-bouncer.yaml.local
    systemctl restart crowdsec-custom-bouncer
